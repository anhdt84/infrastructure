module "postgres_module" {
  source            = "./modules/postgres"
  db-username       = var.db-username
  db-password       = var.db-password
  db-admin-password = var.db-admin-password
  db-name           = var.db-name
}
