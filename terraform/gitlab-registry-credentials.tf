module "gitlab-registry-credentials" {
  source          = "./modules/registry-credentials"
  docker-username = var.gitlab-docker-username
  docker-password = var.gitlab-docker-password
  docker-server   = var.gitlab-docker-server
  docker-email    = var.gitlab-docker-email
}
